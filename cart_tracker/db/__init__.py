from db.influx_manager import InfluxManager
from db.mongodb_manager import MongoManager

# init singleton db

influx = InfluxManager("qr_codes")
mongo = MongoManager()
