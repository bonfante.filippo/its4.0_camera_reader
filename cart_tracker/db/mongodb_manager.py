from pymongo import collection, MongoClient

from config.connect_string_manager import get_connection_params


class MongoManager:
    mongodb_uri = get_connection_params("MONGODB_URI")
    receipt_collection: collection.Collection = None
    mongo_class_client: MongoClient = None

    def __init__(self):
        self.pymongo_connect(MongoManager.mongodb_uri)

    @staticmethod
    def pymongo_connect(mongodb_uri: str):
        '''
        Method used when accessing data from db
        Try to connect to mongodb at specified uri
        :param mongodb_uri: uri mongodb
        :return: void
        '''
        try:
            client = MongoClient(mongodb_uri)
            db = client.its40
            MongoManager.mongo_class_client = client
            MongoManager.receipt_collection = db.receipts
            print("[MONGODB - PyMongo] Successfully connected to db.")
        except Exception as ex:
            print("[MONGODB] Connection error, ", ex)

    @classmethod
    def insert_document(cls, document):
        try:
            cls.receipt_collection.insert_one(document)
            print("[MONGODB] Successfully INSERT on db.")
        except Exception as e:
            print("[MONGODB] Insert error , ", e)
            pass
