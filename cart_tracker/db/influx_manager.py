from influxdb import InfluxDBClient

from config.connect_string_manager import get_connection_params


class InfluxManager:
    infl_host = get_connection_params("INFL_HOST")

    def __init__(self, database_name):
        try:
            self.client = InfluxDBClient(self.infl_host, 8086, database=database_name)
            self.__create_db(database_name)

            print("[INFLUX] connected.")
        except Exception:
            print("[INFLUX] ERROR while connection.")
            pass

    def __create_db(self, db_name: str):
        try:
            db_list = self.client.get_list_database()
            db_found = False

            for db in db_list:
                if db['name'] == db_name:
                    db_found = True
                    print(f"[INFLUX] DB with name {db_name} found")
                if not db_found:
                    print(f"[INFLUX] DB with name {db_name} with not found.")
                    self.client.create_database(db_name)
        except Exception as e:
            print(f"[INFLUX] Error creating {db_name}.")
            raise e
