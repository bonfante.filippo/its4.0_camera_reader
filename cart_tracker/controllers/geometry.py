from models.perimeter import Perimeter
from models.rect import Rect
from models.point import Point
from models.zone import Zone


class Geometry:
    @staticmethod
    def rect_in_rect(r: Rect, t: Rect) -> bool:
        '''
         assume origin on Top Left corner
         a,b are the top-left coordinate of the rectangle and (c,d) be its width and height.
        o judge a point(x0,y0) is in the rectangle, just to check if a < x0 < a+c and b < y0 < b + d
        or
        if r.Left is lower than t.Left the r rect is not inside the t rect and so on

        find centroid and see if it is inside the rect
        :param r: rect
        :param t: other rect
        :return: bool if rect in rect
        '''
        p = Point()
        p.x = r.left + r.width / 2
        p.y = r.top + r.height / 2

        if p.x < t.left:
            return False

        if p.y < t.top:
            return False
        if p.x > t.left + t.width:
            return False
        if p.y > t.top + t.height:
            return False

        return True

    @staticmethod
    def rect_to_zone(r: Rect, zone_list: [Zone]):
        for zone in zone_list:

            if Geometry.rect_in_rect(r, zone.rect):
                return zone
        return None

    @staticmethod
    def load_perimeter(file_name: str) -> Perimeter:
        '''
         1. load a JSON containing the Perimeter object
             or
         2. return embedded data as Perimeter object
        :param file_name: file to load zone configuration
        :return: perimeter
        '''
        r = Rect
        r.top = 0
        r.left = 0
        r.width = 1280
        r.height = 720

        zone = Geometry.get_zones()

        p = Perimeter()
        p.rect = r
        p.zones = zone

        return p

    @staticmethod
    def get_zones() -> [Zone]:
        '''
        le zone contrassegnate con "e" sono non calpestabili
         riferirsi al foglio RectGen.xlsm
         una volta disegnata la mappa, generare il codice, copiarlo e incollarlo
        qui sotto per popolare la lista
        :return:
        '''
        zone_list: [Zone] = {
            Zone(id=1, name="galleria", rect=Rect(left=0, top=0, width=128, height=720)),
            Zone(id=2, name="e", rect=Rect(left=128, top=0, width=96, height=120)),
            Zone(id=3, name="corridoio1", rect=Rect(left=224, top=24, width=64, height=672)),
            Zone(id=5, name="pet", rect=Rect(left=288, top=24, width=288, height=48)),
            Zone(id=6, name="corridoio 2", rect=Rect(left=576, top=24, width=64, height=624)),
            Zone(id=7, name="snacs", rect=Rect(left=640, top=24, width=160, height=96)),
            Zone(id=8, name="corridoio3", rect=Rect(left=800, top=24, width=64, height=312)),
            Zone(id=9, name="birre", rect=Rect(left=864, top=24, width=192, height=96)),
            Zone(id=10, name="vini", rect=Rect(left=1056, top=24, width=224, height=192)),
            Zone(id=11, name="e", rect=Rect(left=288, top=72, width=288, height=96)),
            Zone(id=12, name="cassa", rect=Rect(left=128, top=120, width=96, height=288)),
            Zone(id=13, name="e", rect=Rect(left=640, top=120, width=160, height=48)),
            Zone(id=14, name="e", rect=Rect(left=864, top=120, width=128, height=48)),
            Zone(id=15, name="corridoio 4", rect=Rect(left=992, top=120, width=64, height=576)),
            Zone(id=16, name="corisa 7", rect=Rect(left=288, top=168, width=288, height=48)),
            Zone(id=17, name="corisa 8", rect=Rect(left=640, top=168, width=160, height=48)),
            Zone(id=18, name="promo 2", rect=Rect(left=864, top=168, width=128, height=48)),
            Zone(id=19, name="e", rect=Rect(left=288, top=216, width=288, height=72)),
            Zone(id=20, name="e", rect=Rect(left=640, top=216, width=160, height=72))
            ,
            Zone(id=21, name="e", rect=Rect(left=864, top=216, width=128, height=72))
            ,
            Zone(id=22, name="servito 1", rect=Rect(left=1056, top=216, width=224, height=192))
            ,
            Zone(id=23, name="corsia 5", rect=Rect(left=288, top=288, width=288, height=48))
            ,
            Zone(id=24, name="corsia 6", rect=Rect(left=640, top=288, width=160, height=48)),
            Zone(id=25, name="promo 1", rect=Rect(left=864, top=288, width=128, height=48))
            ,
            Zone(id=26, name="e", rect=Rect(left=288, top=336, width=288, height=72))
            ,
            Zone(id=27, name="e", rect=Rect(left=640, top=336, width=352, height=72))
            ,
            Zone(id=28, name="e", rect=Rect(left=128, top=408, width=96, height=96))
            ,
            Zone(id=29, name="corsia 3", rect=Rect(left=288, top=408, width=288, height=48)),
            Zone(id=30, name="corsia 4", rect=Rect(left=640, top=408, width=352, height=48))

        }
        return zone_list







        '''
        
        Zone
        (id=15, name="corridoio 4", Rect=
        Rect
        (left = 992, top = 120, width = 64, height = 576))
        ,
        Zone
        (id=16, name="corisa 7", Rect=
        Rect
        (left = 288, top = 168, width = 288, height = 48))
        ,
        Zone
        (id=17, name="corisa 8", Rect=
        Rect
        (left = 640, top = 168, width = 160, height = 48))
        ,
        Zone
        (id=18, name="promo 2", Rect=
        Rect
        (left = 864, top = 168, width = 128, height = 48))
        ,
        Zone
        (id=19, name="e", Rect=
        Rect
        (left = 288, top = 216, width = 288, height = 72))
        ,
        Zone
        (id=20, name="e", Rect=
        Rect
        (left = 640, top = 216, width = 160, height = 72))
        ,
        Zone
        (id=21, name="e", Rect=
        Rect
        (left = 864, top = 216, width = 128, height = 72))
        ,
        Zone
        (id=22, name="servito 1", Rect=
        Rect
        (left = 1056, top = 216, width = 224, height = 192))
        ,
        Zone
        (id=23, name="corsia 5", Rect=
        Rect
        (left = 288, top = 288, width = 288, height = 48))
        ,
        Zone
        (id=24, name="corsia 6", Rect=
        Rect
        (left = 640, top = 288, width = 160, height = 48))
        ,
        Zone
        (id=25, name="promo 1", Rect=
        Rect
        (left = 864, top = 288, width = 128, height = 48))
        ,
        Zone
        (id=26, name="e", Rect=
        Rect
        (left = 288, top = 336, width = 288, height = 72))
        ,
        Zone
        (id=27, name="e", Rect=
        Rect
        (left = 640, top = 336, width = 352, height = 72))
        ,
        Zone
        (id=28, name="e", Rect=
        Rect
        (left = 128, top = 408, width = 96, height = 96))
        ,
        Zone
        (id=29, name="corsia 3", Rect=
        Rect
        (left = 288, top = 408, width = 288, height = 48))
        ,
        Zone
        (id=30, name="corsia 4", Rect=
        Rect
        (left = 640, top = 408, width = 352, height = 48))
        ,
        Zone
        (id=31, name="servito 2", Rect=
        Rect
        (left = 1056, top = 408, width = 224, height = 168))
        ,
        Zone
        (id=32, name="e", Rect=
        Rect
        (left = 288, top = 456, width = 288, height = 72))
        ,
        Zone
        (id=33, name="e", Rect=
        Rect
        (left = 640, top = 456, width = 352, height = 72))
        ,
        Zone
        (id=34, name="ingresso", Rect=
        Rect
        (left = 128, top = 504, width = 96, height = 144))
        ,
        Zone
        (id=35, name="corsia 1", Rect=
        Rect
        (left = 288, top = 528, width = 288, height = 48)}
        ,
        Zone
        (id=36, name="corsia 2", Rect=
        Rect
        (left = 640, top = 528, width = 352, height = 48}}
        ,
        Zone
        (id = 37, name = "e", Rect =
        Rect
        (left = 288, top = 576, width = 288, height = 72}}
        ,
        Zone
        (id = 38, name = "e", Rect =
        Rect
        (left = 640, top = 576, width = 352, height = 72}}
        ,
        Zone
        (id = 39, name = "servito 3", Rect =
        Rect
        (left = 1056, top = 576, width = 224, height = 120}}
        ,
        Zone
        (id = 40, name = "e", Rect =
        Rect
        (left = 128, top = 648, width = 96, height = 72}}
        ,
        Zone
        (id = 41, name = "acque", Rect =
        Rect
        (left = 288, top = 648, width = 704, height = 48}}
        ,
        Zone
        (id = 42, name = "e", Rect =
        Rect
        (left = 224, top = 696, width = 1056, height = 24}}
        }'''
