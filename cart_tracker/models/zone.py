from models.rect import Rect


class Zone:
    id: int
    name: str
    rect: Rect

    def __init__(self, id, name, rect):
        self.id = id
        self.name = name
        self.rect = rect
