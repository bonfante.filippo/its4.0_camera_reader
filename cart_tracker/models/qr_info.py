from models.rect import Rect
import datetime


class QRInfo:
    qr_id: int
    zone_id: int
    rect: Rect
    timestamp: datetime
