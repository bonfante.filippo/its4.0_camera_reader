from models.rect import Rect
from models.zone import Zone


class Perimeter:
    rect: Rect
    zones: [Zone]
