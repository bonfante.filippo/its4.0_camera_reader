class Rect:
    left: int
    top: int
    width: int
    height: int

    def __init__(self, left, top, width, height):
        self.left = left
        self.top = top
        self.width = width
        self.height = height

