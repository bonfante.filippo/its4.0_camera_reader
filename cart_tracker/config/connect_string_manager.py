import configparser
import os


def get_connection_params(connection_string_name: str):
    '''
    Given the string identifier on config.ini, retrieve the string
    :param connection_string_name: string identifier
    :return: connection string
    '''
    # parser for config.ini file
    config = configparser.ConfigParser()

    # get default path for config.ini to avoid KeyError exception
    config.read(os.path.abspath("config/config.ini"))
    return config['DEFAULT'][connection_string_name]
