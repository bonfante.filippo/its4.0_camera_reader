import {AfterViewInit, Component, OnChanges, OnInit, SimpleChange, SimpleChanges, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {HttpClient} from '@angular/common/http';
import {Table} from 'bootstrap';


@Component({
  selector: 'app-s-percorsi',
  templateUrl: './s-percorsi.component.html',
  styleUrls: ['./s-percorsi.component.css']
})

export class SPercorsiComponent implements OnInit {

  data: Paths[] = [];
  percorsi = 'percorsi';
  rank = [];
  zoneSequence = [];
  avgTime = [];
  numCarts = [];
  PATH_DATA: Paths[];
  url = 'https://its40apiv1.azurewebsites.net/api/cartPath/12';
  tempObj: Paths;

  headElements = ['Rank', 'Zone Sequence', 'Permanence time', 'Carts'];

  constructor(private http: HttpClient) {
  }
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  ngOnInit() {
    this.getData();
  }
    getData() {
    this.http.get(this.url)
      .subscribe((res: Paths[]) => {
        for (const item of res) {
          this.tempObj = {
            rank: item.rank,
            zoneSequence: item.zoneSequence,
            avgTime: item.avgTime,
            numCarts: item.numCarts
          };
          console.log(this.tempObj.avgTime);
          this.data.push(this.tempObj);
        }
      });
    }
}
export interface Paths {
  rank: number;
  zoneSequence: string;
  avgTime: string;
  numCarts: number;
}

