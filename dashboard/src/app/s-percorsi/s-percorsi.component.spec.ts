import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SPercorsiComponent } from './s-percorsi.component';

describe('SPercorsiComponent', () => {
  let component: SPercorsiComponent;
  let fixture: ComponentFixture<SPercorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SPercorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPercorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
