import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {reject, resolve} from 'q';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  params;

  constructor(private http: HttpClient) {
  }

  fetchData() {
    this.http.get('http://5d079d45fa0025001457824b.mockapi.io/api/percorsi')
      .toPromise()
      .then(
        res => { // Success
          this.params = res;
        },
      );
    return this.params;
  }
}



// namespace its40api.DataAccess
// {
//   using System;
//   using System.Collections.Generic;
//   public class CartPath
//   {
//     public int Rank { get; set; }
// public string ZoneSequence { get; set; }
// public List<ZoneTime> Zones { get; set; }
// public TimeSpan AvgTime { get; set; }
//   double NumCarts { get; set; }
// }
// public class ZoneTime
// {
//   public int ZoneId { get; set; }
// public TimeSpan AvgTime { get; set; }
// }
// }
