import {Component, EventEmitter, Output} from '@angular/core';
import {DataService} from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'its-dashboard';

  @Output() chartParams = new EventEmitter<any>();

  myData = [
    {
      name: 'Real Time Percorsi',
      link: 'rt-percorsi'
    },
    {
      name: 'Storico Percorsi',
      link: 's-percorsi'
    },
       {
      name: 'Real Time Zone',
      link: 'rt-zone'
    },
       {
      name: 'Storico Zone',
      link: 's-zone'
    },
       {
      name: 'Storico Carrelli',
      link: 's-carrelli'
    },
  ];

  message: any;
  params: any;
  resendParams() {
    this.chartParams.emit(this.message);
  }

  receiveParams($event) {
    this.message = $event;
    this.resendParams();
  }

  constructor(dataService: DataService) {
    this.params = this.message;

    dataService.setOption('recivedParams', this.params);
  }

}
