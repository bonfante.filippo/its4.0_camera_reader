import {Component, Input, OnInit} from '@angular/core';
import {Paths} from '../rt-percorsi/rt-percorsi.component';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  data: Paths[] = [];
  tempObj: Paths;
  url = 'https://its40apiv1.azurewebsites.net/api/cartPath/12';
  rank;
  zoneSequence;
  avgTime;
  numCarts;
  @Input() tipo: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    console.log(this.tipo);
    if (this.tipo !== 'percorsi') {
      return;
    }
    this.getPercorsi();
  }

  getPercorsi() {
    this.data = [];
    this.http.get(this.url)
      .subscribe((res) => {
        for (let i = 0; i <= 10; i++) {
          // this.tempObj = {
          this.rank.push(res[i].rank);
          this.zoneSequence.push(res[i].zoneSequence);
          this.avgTime.push(res[i].avgTime);
          this.numCarts.push(res[i].numCarts);
          // };
          this.data.push(this.tempObj);
        }
      });
    const barChartOptions = {
      scaleShowVerticalLines: false,
      responsive: true
    };
    const barChartLabels = this.zoneSequence;
    const barChartType = 'bar';
    const barChartLegend = true;
    const barChartData = [
      {data: this.avgTime, label: 'Series A'},
      {data: this.numCarts, label: 'Series B'}
    ];
  }
}
