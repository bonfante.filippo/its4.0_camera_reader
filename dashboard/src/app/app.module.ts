import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {SPercorsiComponent} from './s-percorsi/s-percorsi.component';
import {RtPercorsiComponent} from './rt-percorsi/rt-percorsi.component';
import { ChartsModule } from 'ng2-charts';
import { RtZoneComponent } from './rt-zone/rt-zone.component';
import { SZoneComponent } from './s-zone/s-zone.component';
import { SCarrelliComponent } from './s-carrelli/s-carrelli.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DataButtonComponent } from './data-button/data-button.component';
import { ChartComponent } from './chart/chart.component';
import {ConfigService} from './config.service';
import { MatFormFieldModule, MatInputModule, MatPaginatorModule, MatTableModule, MatSortModule } from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SPercorsiComponent,
    RtPercorsiComponent,
    RtZoneComponent,
    SZoneComponent,
    SCarrelliComponent,
    DataButtonComponent,
    ChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ChartsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    CdkTableModule,
    RouterModule.forRoot([
      {
        path: '',
        component: RtPercorsiComponent
      },
      {
        path: 'rt-percorsi',
        component: RtPercorsiComponent
      },
      {
        path: 's-percorsi',
        component: SPercorsiComponent
      },
      {
        path: 'rt-zone',
        component: RtZoneComponent
      },
      {
        path: 's-zone',
        component: SZoneComponent
      },
      {
        path: 's-carrelli',
        component: SCarrelliComponent
      },
    ]),
  ],
  providers: [ ConfigService],
  bootstrap: [AppComponent]
})
export class AppModule {

}

export interface DataModel {
  letter: string;
  frequency: number;
}
