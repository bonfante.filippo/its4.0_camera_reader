import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {HttpClient} from '@angular/common/http';
import {Table} from 'bootstrap';

@Component({
  selector: 'app-s-zone',
  templateUrl: './s-zone.component.html',
  styleUrls: ['./s-zone.component.css']
})
export class SZoneComponent implements OnInit {

  data: ZonePerformance[] = [];
  zones: Zone[] = [];
  rank = [];
  zoneSequence = [];
  avgTime = [];
  numCarts = [];
  PATH_DATA: ZonePerformance[];
  url = 'https://its40apiv1.azurewebsites.net/api/zonePerformance/12';
  tempObj: ZonePerformance;
  tempZone: Zone;

  headElements = ['Nome', 'Conversione', 'Totale Speso', 'Tot. Carrelli', 'Permanenza Media'];

  constructor(private http: HttpClient) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.getData();
    this.getZonesName();
    this.PATH_DATA = this.data;
  }

  getData() {
    this.http.get(this.url)
      .subscribe((res: ZonePerformance[]) => {
        for (const item of res) {
          this.tempObj = {
            zoneId: item.zoneId,
            conversion: item.conversion,
            totalSpending: item.totalSpending,
            numCarts: item.numCarts,
            avgStayTime: item.avgStayTime,
          };
          this.data.push(this.tempObj);
        }
      });
  }

  getZonesName() {
    this.http.get('https://its40apiv1.azurewebsites.net/api/zone')
      .subscribe((res: Zone[]) => {
        for (const item of res) {
          this.tempZone = {
            zoneId: item.zoneId,
            name: item.name,
          };
          this.zones.push(this.tempZone);
        }
      });
  }
}

export interface ZonePerformance {
  zoneId: number;
  conversion: number;
  totalSpending: number;
  numCarts: number;
  avgStayTime: string;
}
export interface Zone {
  zoneId: number;
  name: string;
}
