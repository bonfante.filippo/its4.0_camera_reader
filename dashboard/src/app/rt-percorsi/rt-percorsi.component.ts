import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {HttpClient} from '@angular/common/http';
import {Table} from 'bootstrap';


@Component({
  selector: 'app-rt-percorsi',
  templateUrl: './rt-percorsi.component.html',
  styleUrls: ['./rt-percorsi.component.css']
})

export class RtPercorsiComponent implements OnInit {

  data: Paths[] = [];
  rank = [];
  zoneSequence = [];
  avgTime = [];
  numCarts = [];
  PATH_DATA: Paths[];
  url = 'https://its40apiv1.azurewebsites.net/api/cartPath/12';
  tempObj: Paths;

  headElements = ['Rank', 'Zone Sequence', 'Permanence time', 'Carts'];

  constructor(private http: HttpClient) {
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.getData();
    setTimeout(() => this.getData(), 10000);
    this.PATH_DATA = this.data;
  }

  getData() {
    this.data = [];
    this.http.get(this.url)
      .subscribe((res: Paths[]) => {
        for (const item of res) {
          this.tempObj = {
            rank: item.rank,
            zoneSequence: item.zoneSequence,
            avgTime: item.avgTime,
            numCarts: item.numCarts
          };
          console.log(this.tempObj.avgTime);
          this.data.push(this.tempObj);
        }
      });


  }
}

export interface Paths {
  rank: number;
  zoneSequence: string;
  avgTime: string;
  numCarts: number;
}

