import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RtPercorsiComponent } from './rt-percorsi.component';

describe('RtPercorsiComponent', () => {
  let component: RtPercorsiComponent;
  let fixture: ComponentFixture<RtPercorsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RtPercorsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RtPercorsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
