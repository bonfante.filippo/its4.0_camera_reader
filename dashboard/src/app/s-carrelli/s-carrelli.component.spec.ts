import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SCarrelliComponent } from './s-carrelli.component';

describe('SCarrelliComponent', () => {
  let component: SCarrelliComponent;
  let fixture: ComponentFixture<SCarrelliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SCarrelliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SCarrelliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
