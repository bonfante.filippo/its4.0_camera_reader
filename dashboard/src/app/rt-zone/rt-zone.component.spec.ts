import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RtZoneComponent } from './rt-zone.component';

describe('RtZoneComponent', () => {
  let component: RtZoneComponent;
  let fixture: ComponentFixture<RtZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RtZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RtZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
