import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from '../data.service';

@Component({
  selector: 'app-data-button',
  templateUrl: './data-button.component.html',
  styleUrls: ['./data-button.component.css']
})
export class DataButtonComponent implements OnInit {
  constructor(dataService: DataService) {

    // dataService.setOption('recivedParams', this.params);
  }
  public barChartOptions: {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: ['Percorso 1', 'Percorso 2', 'Percorso 3', 'Percorso 4'];
  public barChartType: 'bar';
  public barChartLegend: true;
  public barChartData: [
    {
      data: [50, 70, 46, 73],
      label: 'Numero Carrello'
    },
    {
      data: [44, 34, 23, 85],
      label: 'Permanenza Media'
    }
    ];
  @Input()
  dato: any;

  // @Input()
  // chartParams: any;

  @Output() messageEvent = new EventEmitter<any>();

  ngOnInit() {
  }

  // getParams() {
  //   const params = {

}
