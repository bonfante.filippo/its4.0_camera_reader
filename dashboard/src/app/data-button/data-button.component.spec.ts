import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataButtonComponent } from './data-button.component';

describe('DataButtonComponent', () => {
  let component: DataButtonComponent;
  let fixture: ComponentFixture<DataButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
